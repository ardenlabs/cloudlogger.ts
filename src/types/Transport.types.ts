import { LogLevel } from './Log.types';

export interface LogAppender {
    log(message: string, logLevel: LogLevel): void;
}
