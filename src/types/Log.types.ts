/**
 * LogLevel's are added verbatum from Winston's implementation of RFC5424
 */
export enum LogLevel {
    metric = 0,
    alert = 1,
    error = 2,
    warn = 3,
    info = 4,
    http = 5,
    verbose = 6,
    debug = 7,
    silly = 8,
}

export const LogLevelValue = ['METRIC', 'ALERT', 'ERROR', 'WARNING', 'INFO', 'HTTP', 'VERBOSE', 'DEBUG', 'SILLY'];

export interface LogPayload {
    level: LogLevel;
    message?: string; // Message to associate with the logged payload.
    timestamp: Date; // UTC Date: 2021-08-15T01:36:39.683Z
    // eslint-disable-next-line @typescript-eslint/ban-types
    payload?: Object; // Payload of an object to surface.
}
