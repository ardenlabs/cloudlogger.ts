import { HttpData } from './HttpLog.types';
import { LogLevel } from './Log.types';
import { MetricData } from './MetricLog.types';
import { LogAppender } from './Transport.types';

/* eslint-disable @typescript-eslint/ban-types */
export interface ILogger {
    metric(metricData: MetricData, loggerName?: string): void;
    http(request: HttpData, response: HttpData, durationMs: number, loggerName?: string): void;
    info(message: string, payload?: Object, loggerName?: string): void;
    warn(message: string, payload?: Object, loggerName?: string): void;
    error(message: string, payload?: Object, loggerName?: string): void;
    verbose(message: string, payload?: Object, loggerName?: string): void;
    silly(message: string, payload?: Object, loggerName?: string): void;
    debug(message: string, payload?: Object, loggerName?: string): void;
    alert(message: string, payload?: Object, loggerName?: string): void;
    clear(): void;
    flush(): void;
}

export interface RootLoggingConfig {
    // Name to apply for all Logs
    name?: string;
    // Minimum LogLevel to emit Messages.
    filterLogLevel?: LogLevel; // Default LogLevel.info
    // Flag to indicate if filtered messages should be retained for emitting when error encountered.
    flushOnError?: boolean; // Default: false
    // The amount of filtered logs to retain in the ringBuffer when flushOnError is set to true.
    maxBufferSize?: number; // Default: 50.
    // Flag to indicate whether to suppress all Logs
    silent?: boolean; // Default: false
    // Delimitter to use when seperating log names.
    delimitter?: string; // Default is "::"
    // LoggingAppenders to use with CloudLogger
    appenders?: LogAppender[]; // Default is: ConsoleAppender
}

export interface LoggerConfig {
    name?: string;
}
