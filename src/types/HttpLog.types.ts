/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/ban-types */
export interface HttpData {
    headers?: {
        [key: string]: string;
    };
    body?: any;
    params?: {
        [key: string]: string;
    };
    status?: number;
    url?: string;
}

export interface HttpPayload {
    request?: HttpData;
    response?: HttpData;
    durationMs: number;
}
