export type MetricType =
    | 'Seconds'
    | 'Microseconds'
    | 'Milliseconds'
    | 'Bytes'
    | 'Kilobytes'
    | 'Megabytes'
    | 'Gigabytes'
    | 'Terabytes'
    | 'Bits'
    | 'Kilobits'
    | 'Megabits'
    | 'Gigabits'
    | 'Terabits'
    | 'Percent'
    | 'Count'
    | 'Bytes/Second'
    | 'Kilobytes/Second'
    | 'Megabytes/Second'
    | 'Gigabytes/Second'
    | 'Terabytes/Second'
    | 'Bits/Second'
    | 'Kilobits/Second'
    | 'Megabits/Second'
    | 'Gigabits/Second'
    | 'Terabits/Second'
    | 'Count/Second'
    | 'None';

export interface MetricData {
    metricType: MetricType;
    metricNameSpace: string;
    metricName: string;
    metricValue: number;
    dimensions: {
        [name: string]: string;
    };
}
