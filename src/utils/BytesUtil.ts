import sizeof from 'object-sizeof';

// Reference to original: https://gist.github.com/lanqy/5193417
export function calculateSizeFromBytes(bytes: number): string {
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (!bytes || bytes == 0) {
        return `0 ${sizes[0]}`;
    }
    const i = Math.floor(Math.log(bytes) / Math.log(1024));
    if (i == 0) {
        return `${bytes} ${sizes[i]}`;
    } else {
        return `${(bytes / Math.pow(1024, i)).toFixed(1)} ${sizes[i]}`;
    }
}

export function calculateSizeFromPayload<T>(payload: T): string {
    return calculateSizeFromBytes(sizeof<T>(payload));
}
