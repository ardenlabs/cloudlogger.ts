/* eslint-disable @typescript-eslint/ban-types */
import { CoreLogger } from './CoreLogger';
import { Logger } from './Logger';
import { ILogger, LoggerConfig, RootLoggingConfig } from '../types/Logger.types';

export class LoggerFactory {
    coreLogger: CoreLogger;

    constructor(options: RootLoggingConfig) {
        this.coreLogger = new CoreLogger(options);
    }

    getLogger(option?: LoggerConfig): ILogger {
        const logger = new Logger(option?.name, this.coreLogger);
        return logger;
    }
}
