/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/ban-types */
import { HttpData } from '../types/HttpLog.types';
import { ILogger } from '../types/Logger.types';
import { MetricData } from '../types/MetricLog.types';

/**
 * This Logger is a wrapper around CoreLogger. It's purpose is to append it's name over CoreLogger without exposing the
 * name property.
 */
export class Logger implements ILogger {
    logger: ILogger;
    name: string;
    constructor(name: string, logger: ILogger) {
        this.name = name;
        this.logger = logger;
    }
    metric(metricData: MetricData): void {
        this.logger.metric(metricData, this.name);
    }
    http(request: HttpData, response: HttpData, durationMs: number): void {
        this.logger.http(request, response, durationMs, this.name);
    }
    info(message: string, payload?: Object): void {
        this.logger.info(message, payload, this.name);
    }
    warn(message: string, payload?: Object): void {
        this.logger.warn(message, payload, this.name);
    }
    error(message: string, payload?: Object): void {
        this.logger.error(message, payload, this.name);
    }
    verbose(message: string, payload?: Object): void {
        this.logger.verbose(message, payload, this.name);
    }
    silly(message: string, payload?: Object): void {
        this.logger.silly(message, payload, this.name);
    }
    debug(message: string, payload?: Object): void {
        this.logger.debug(message, payload, this.name);
    }
    alert(message: string, payload?: Object): void {
        this.logger.alert(message, payload, this.name);
    }
    clear(): void {
        this.logger.clear();
    }
    flush(): void {
        this.logger.flush();
    }
}
