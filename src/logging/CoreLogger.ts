/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/ban-types */
import { HttpData } from '../types/HttpLog.types';
import { LogLevel, LogLevelValue, LogPayload } from '../types/Log.types';
import { ILogger, RootLoggingConfig } from '../types/Logger.types';
import { MetricData } from '../types/MetricLog.types';
import { calculateSizeFromPayload } from '../utils/BytesUtil';

import { circular_buffer } from 'circular_buffer_js';
import { LogAppender } from '../types/Transport.types';
import { ConsoleAppender } from '../appenders/Console';

export class CoreLogger implements ILogger {
    readonly name: string;
    readonly logLevel: LogLevel;
    readonly flushOnError: boolean;
    readonly silent: boolean;
    readonly delimitter: string;
    ringBuffer: circular_buffer<LogPayload>;
    logAppenders: LogAppender[];
    constructor(options: RootLoggingConfig) {
        this.name = options.name;
        this.flushOnError = options?.flushOnError || false;
        this.logLevel = options?.filterLogLevel || LogLevel.info;
        this.silent = options?.silent || false;
        this.delimitter = options?.delimitter || '::';
        if (this.flushOnError) {
            this.ringBuffer = new circular_buffer(options?.maxBufferSize || 50);
        }
        this.logAppenders = options?.appenders || [new ConsoleAppender()];
    }

    public metric(metricData: MetricData, loggerName?: string): void {
        const metricPayload = {
            logName: [this.name, loggerName].filter(x => !!x).join(this.delimitter),
            level: LogLevel.metric,
            timestamp: new Date(Date.now()),
            payload: metricData,
        };
        this.log(metricPayload);
    }

    public http(request: HttpData, response: HttpData, durationMs: number, loggerName?: string): void {
        const httpRequest = {
            url: request.url,
            headers: {
                ...request.headers,
                Authorization: '<REDACTED>',
                buffer: '<REDACTED>',
            },
            params: request.params,
            body: request.body,
        };
        const httpResponse = {
            headers: {
                ...response.headers,
                Authorization: '<REDACTED>',
                buffer: '<REDACTED>',
            },
            status: response.status,
            body: response.body,
        };
        const httpPayload = {
            logName: [this.name, loggerName].filter(x => !!x).join(this.delimitter),
            level: LogLevel.http,
            timestamp: new Date(Date.now()),
            payload: {
                request: httpRequest,
                response: httpResponse,
                durationMs,
            },
        };
        this.log(httpPayload);
    }

    public info(message: string, payload?: Object, loggerName?: string): void {
        const logPayload = {
            logName: [this.name, loggerName].filter(x => !!x).join(this.delimitter),
            level: LogLevel.info,
            timestamp: new Date(Date.now()),
            message,
            payload,
        };
        this.log(logPayload);
    }

    public error(message: string, payload?: Object, loggerName?: string): void {
        const logPayload = {
            logName: [this.name, loggerName].filter(x => !!x).join(this.delimitter),
            level: LogLevel.error,
            timestamp: new Date(Date.now()),
            message,
            payload,
        };
        this.log(logPayload);
    }

    public warn(message: string, payload?: Object, loggerName?: string): void {
        const logPayload = {
            logName: [this.name, loggerName].filter(x => !!x).join(this.delimitter),
            level: LogLevel.warn,
            timestamp: new Date(Date.now()),
            message,
            payload,
        };
        this.log(logPayload);
    }

    public verbose(message: string, payload?: Object, loggerName?: string): void {
        const logPayload = {
            logName: [this.name, loggerName].filter(x => !!x).join(this.delimitter),
            level: LogLevel.verbose,
            timestamp: new Date(Date.now()),
            message,
            payload,
        };
        this.log(logPayload);
    }

    public silly(message: string, payload?: Object, loggerName?: string): void {
        const logPayload = {
            logName: [this.name, loggerName].filter(x => !!x).join(this.delimitter),
            level: LogLevel.silly,
            timestamp: new Date(Date.now()),
            message,
            payload,
        };
        this.log(logPayload);
    }

    public debug(message: string, payload?: Object, loggerName?: string): void {
        const logPayload = {
            logName: [this.name, loggerName].filter(x => !!x).join(this.delimitter),
            level: LogLevel.debug,
            timestamp: new Date(Date.now()),
            message,
            payload,
        };
        this.log(logPayload);
    }

    public alert(message: string, payload?: Object, loggerName?: string): void {
        const logPayload = {
            logName: [this.name, loggerName].filter(x => !!x).join(this.delimitter),
            level: LogLevel.alert,
            timestamp: new Date(Date.now()),
            message,
            payload,
        };
        this.log(logPayload);
    }

    public clear(): void {
        this.ringBuffer.clear();
    }

    public flush(): void {
        // Flush entire Log Buffer:
        while (!this.ringBuffer.isEmpty) {
            this.flushPayload(this.ringBuffer.pop());
        }
    }

    // Helper function for managing Log state of payloads.
    private log(logPayload: LogPayload): void {
        if (this.silent) {
            return; // No-opp.
        } else if (logPayload.level == LogLevel.error && this.flushOnError) {
            this.flush();
            this.flushPayload(logPayload);
        } else if (logPayload.level <= this.logLevel) {
            this.flushPayload(logPayload);
        } else if (this.flushOnError) {
            if (this.ringBuffer.isFull) {
                this.ringBuffer.pop();
            }
            this.ringBuffer.push(logPayload);
        }
    }
    // Helper function for outputting LogPayloads in a consistent format.
    private flushPayload(logPayload: LogPayload) {
        const logMessage = JSON.stringify(
            {
                ...logPayload,
                size: calculateSizeFromPayload<LogPayload>(logPayload),
                // Overwrite values with human readable text:
                level: LogLevelValue[logPayload.level],
            },
            null,
            2,
        );
        // Emit the log across all LogAppenders
        for (const logAppender of this.logAppenders) {
            logAppender.log(logMessage, logPayload.level);
        }
    }
}
