import { LogLevel } from '../types/Log.types';
import { LogAppender } from '../types/Transport.types';

export class ConsoleAppender implements LogAppender {
    public log(message: string, logLevel: LogLevel): void {
        switch (logLevel) {
            case LogLevel.metric:
                console.info(message);
                return;
            case LogLevel.alert:
                console.info(message);
                return;
            case LogLevel.error:
                console.error(message);
                return;
            case LogLevel.warn:
                console.warn(message);
                return;
            case LogLevel.info:
                console.info(message);
                return;
            case LogLevel.http:
            case LogLevel.verbose:
            case LogLevel.debug:
            case LogLevel.silly:
                console.debug(message);
                return;
            default:
                // No Log output on bad usage.
                return;
        }
    }
}
