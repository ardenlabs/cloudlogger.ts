import { getLogger } from './LogUtil';
const logger = getLogger({ name: 'main.ts' });

const robotsText = `
User-agent: *
Disallow: /
`;

function handler() {
    const response = {
        statusCode: 200,
        headers: {
            'Cache-Control': 'max-age=100',
            'Content-Type': 'text/plain',
            'Content-Encoding': 'UTF-8',
        },
        isBase64Encoded: false,
        body: robotsText,
    };
    logger.info('Returning response.', response);
    return response;
}

module.exports.handler = handler;
