// src/utils/LoggerUtil.ts

// Types
import { ILogger, LoggerConfig } from '../../src/types/Logger.types';
import { LogLevel } from '../../src/types/Log.types';
// Dependencies
import { LoggerFactory } from '../../src/logging/LogFactory';

const factory = new LoggerFactory({
    // Use the Lambda's function name:
    // ref: https://docs.aws.amazon.com/lambda/latest/dg/configuration-envvars.html#configuration-envvars-runtime
    name: process.env.AWS_LAMBDA_FUNCTION_NAME,
    // Alert logger to use ringBuffer
    flushOnError: true,
    // Allocate space for 100 filtered logPayloads
    maxBufferSize: 100,
    // Filter any message below Info
    filterLogLevel: LogLevel.info,
});

// Export function for usage across code.
export function getLogger(option?: LoggerConfig): ILogger {
    return factory.getLogger(option);
}
