import { LogLevel, LogPayload } from '../src/types/Log.types';
import { LogAppender } from '../src/types/Transport.types';

enum Color {
    Red,
    Green,
    Blue,
}
export const payload = {
    boolean: true,
    dictionary: {
        decimal: 32,
        strArray: ['red', 'blue', 'green'],
    },
    number: 33.2,
    undefined: undefined,
    nullVal: null,
    string: 'StringValue',
    octal: 0o744,
    hex: 0xf00d,
    binary: 0b1010,
    enum: Color.Blue,
};

export class SinkAppender implements LogAppender {
    payloadArray: LogPayload[];
    logCount = 0;
    constructor() {
        this.payloadArray = [];
    }
    public log(message: string, logLevel: LogLevel): void {
        this.payloadArray.push(JSON.parse(message));
        this.logCount += 1;
    }
    public reset(): void {
        this.logCount = 0;
        this.payloadArray = [];
    }
}
