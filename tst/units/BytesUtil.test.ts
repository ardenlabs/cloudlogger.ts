import { calculateSizeFromBytes } from '../../src/utils/BytesUtil';

describe('BytesUtil Validation', () => {
    test('Check Input: null', async () => {
        const result = calculateSizeFromBytes(null);
        expect(result).toEqual('0 Bytes');
    });
    test('Check Input: 0', async () => {
        const result = calculateSizeFromBytes(0);
        expect(result).toEqual('0 Bytes');
    });

    test('Check Input: 1 KB', async () => {
        const result = calculateSizeFromBytes(1024);
        expect(result).toEqual('1.0 KB');
    });

    test('Check Input: 1 MB', async () => {
        const result = calculateSizeFromBytes(1048576);
        expect(result).toEqual('1.0 MB');
    });

    test('Check Input: 1 GB', async () => {
        const result = calculateSizeFromBytes(1073741824);
        expect(result).toEqual('1.0 GB');
    });

    test('Check Input: 1 TB', async () => {
        const result = calculateSizeFromBytes(1099511627776);
        expect(result).toEqual('1.0 TB');
    });
});
