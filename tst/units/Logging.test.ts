import { LogLevel, LogLevelValue, LogPayload } from '../../src/types/Log.types';
import { LoggerFactory } from '../../src/logging/LogFactory';
import { SinkAppender, payload } from '../data';

const sinkAppender = new SinkAppender();

const factory = new LoggerFactory({
    // Use the Lambda's function name:
    // ref: https://docs.aws.amazon.com/lambda/latest/dg/configuration-envvars.html#configuration-envvars-runtime
    name: process.env.AWS_LAMBDA_FUNCTION_NAME,
    // Alert logger to use ringBuffer
    flushOnError: true,
    // Allocate space for 100 filtered logPayloads
    maxBufferSize: 100,
    // Filter any message below Info
    filterLogLevel: LogLevel.silly,
    // Use custom Appender for validation:
    appenders: [sinkAppender],
});

function validatePayload(logPayload: LogPayload, logLevel: LogLevel, message: string) {
    expect(logPayload.level).toEqual(LogLevelValue[logLevel]);
    expect(logPayload.payload).toEqual(payload);
    expect(logPayload.timestamp).toBeDefined();
    expect(logPayload.message).toEqual(message);
}

describe('ValidLogging', () => {
    const logger = factory.getLogger({ name: 'SinkLogger' });
    beforeEach(() => {
        // Reset PayloadArray
        sinkAppender.reset();
    });

    test('Validate Alert', async () => {
        const message = 'Alert';
        logger.alert(message, payload);
        validatePayload(sinkAppender.payloadArray[0], LogLevel.alert, message);
    });

    test('Validate Warning', async () => {
        const message = 'Warning';
        logger.warn(message, payload);
        validatePayload(sinkAppender.payloadArray[0], LogLevel.warn, message);
    });

    test('Validate Info', async () => {
        const message = 'Info';
        logger.info(message, payload);
        validatePayload(sinkAppender.payloadArray[0], LogLevel.info, message);
    });

    test('Validate Debug', async () => {
        const message = 'Debug';
        logger.debug(message, payload);
        validatePayload(sinkAppender.payloadArray[0], LogLevel.debug, message);
    });

    test('Validate Verbose', async () => {
        const message = 'Verbose';
        logger.verbose(message, payload);
        validatePayload(sinkAppender.payloadArray[0], LogLevel.verbose, message);
    });

    test('Validate Silly', async () => {
        const message = 'Silly';
        logger.silly(message, payload);
        validatePayload(sinkAppender.payloadArray[0], LogLevel.silly, message);
    });

    test('Validate Buffer Clear', async () => {
        const message = 'Silly';
        logger.silly(message, payload);
        logger.silly(message, payload);
        expect(sinkAppender.logCount).toEqual(2);
        logger.clear();
        logger;
        validatePayload(sinkAppender.payloadArray[0], LogLevel.silly, message);
    });
});
