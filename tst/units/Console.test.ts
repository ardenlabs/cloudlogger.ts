import { LogLevel, LogLevelValue, LogPayload } from '../../src/types/Log.types';
import { LoggerFactory } from '../../src/logging/LogFactory';
import { payload } from '../data';

const factory = new LoggerFactory({
    // Use the Lambda's function name:
    // ref: https://docs.aws.amazon.com/lambda/latest/dg/configuration-envvars.html#configuration-envvars-runtime
    name: process.env.AWS_LAMBDA_FUNCTION_NAME,
    // Alert logger to use ringBuffer
    flushOnError: true,
    // Allocate space for 100 filtered logPayloads
    maxBufferSize: 100,
    // Filter any message below Info
    filterLogLevel: LogLevel.silly,
});

const logger = factory.getLogger({ name: 'ConsoleLogger' });

function validatePayload(logPayload: LogPayload, logLevel: LogLevel, message: string) {
    expect(logPayload.level).toEqual(LogLevelValue[logLevel]);
    expect(logPayload.payload).toEqual(payload);
    expect(logPayload.timestamp).toBeDefined();
    expect(logPayload.message).toEqual(message);
}

describe('ValidLogging', () => {
    let payloadArray: LogPayload[] = [];
    beforeAll(() => {
        console.info = message => {
            payloadArray.push(JSON.parse(message));
        };
        console.warn = message => {
            payloadArray.push(JSON.parse(message));
        };
        console.error = message => {
            payloadArray.push(JSON.parse(message));
        };
        console.debug = message => {
            payloadArray.push(JSON.parse(message));
        };
    });

    beforeEach(() => {
        // Reset PayloadArray
        payloadArray = [];
    });

    test('Validate Alert', async () => {
        const message = 'Alert';
        logger.alert(message, payload);
        validatePayload(payloadArray[0], LogLevel.alert, message);
    });

    test('Validate Warning', async () => {
        const message = 'Warning';
        logger.warn(message, payload);
        validatePayload(payloadArray[0], LogLevel.warn, message);
    });

    test('Validate Info', async () => {
        const message = 'Info';
        logger.info(message, payload);
        validatePayload(payloadArray[0], LogLevel.info, message);
    });

    test('Validate Debug', async () => {
        const message = 'Debug';
        logger.debug(message, payload);
        validatePayload(payloadArray[0], LogLevel.debug, message);
    });

    test('Validate Verbose', async () => {
        const message = 'Verbose';
        logger.verbose(message, payload);
        validatePayload(payloadArray[0], LogLevel.verbose, message);
    });

    test('Validate Silly', async () => {
        const message = 'Silly';
        logger.silly(message, payload);
        validatePayload(payloadArray[0], LogLevel.silly, message);
    });
});
