import { LogLevel, LogLevelValue, LogPayload } from '../../src/types/Log.types';
import { LoggerFactory } from '../../src/logging/LogFactory';
import { SinkAppender, payload } from '../data';

const sinkAppender = new SinkAppender();

const factory = new LoggerFactory({
    // Use the Lambda's function name:
    // ref: https://docs.aws.amazon.com/lambda/latest/dg/configuration-envvars.html#configuration-envvars-runtime
    name: process.env.AWS_LAMBDA_FUNCTION_NAME,
    // Alert logger to use ringBuffer
    flushOnError: true,
    // Allocate space for 100 filtered logPayloads
    maxBufferSize: 100,
    // Filter any message below Info
    filterLogLevel: LogLevel.info,
    // Use custom Appender for validation:
    appenders: [sinkAppender],
});

function validatePayload(logPayload: LogPayload, logLevel: LogLevel, message: string): void {
    expect(logPayload.level).toEqual(LogLevelValue[logLevel]);
    expect(logPayload.payload).toEqual(payload);
    expect(logPayload.timestamp).toBeDefined();
    expect(logPayload.message).toEqual(message);
}

describe('ValidLogging', () => {
    const logger = factory.getLogger({ name: 'SinkLogger' });
    beforeEach(() => {
        // Reset PayloadArray
        sinkAppender.reset();
    });

    test('Validate Buffer Clear', async () => {
        const message = 'Silly';
        logger.silly(message, payload);
        logger.silly(message, payload);
        expect(sinkAppender.logCount).toEqual(0); // Expect no messages to be emitted
        logger.clear(); // Clear contents
        logger.flush(); // Flush remaining stored messages
        expect(sinkAppender.logCount).toEqual(0); // Expect no stored messages for emission.
    });

    test('Validate Buffer Flush', async () => {
        const message = 'Silly';
        logger.silly(message, payload);
        logger.silly(message, payload);
        expect(sinkAppender.logCount).toEqual(0); // Expect no messages to be emitted
        logger.flush(); // Flush remaining stored messages
        expect(sinkAppender.logCount).toEqual(2); // Expect no stored messages for emission.
    });

    test('Validate Buffer Above Filter', async () => {
        const message = 'Alerting!';
        logger.alert(message, payload);
        logger.alert(message, payload);
        expect(sinkAppender.logCount).toEqual(2); // Expect 2 messages to be emitted.
        logger.flush(); // Flush remaining stored messages
        expect(sinkAppender.logCount).toEqual(2); // Expect no additional logs upon flush.
    });
});
