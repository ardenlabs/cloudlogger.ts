## [1.0.9](https://gitlab.com/ardenlabs/cloudlogger.ts/compare/1.0.8...1.0.9) (2021-10-03)


### Bug Fixes

* corrected types input to src/index.d.ts ([69ae038](https://gitlab.com/ardenlabs/cloudlogger.ts/commit/69ae03866183d3997b9b8028a4a18563f860654f))

## [1.0.8](https://gitlab.com/ardenlabs/cloudlogger.ts/compare/1.0.7...1.0.8) (2021-10-03)


### Features

* added declaration to tsconfig for type exports ([8f9b946](https://gitlab.com/ardenlabs/cloudlogger.ts/commit/8f9b946ad3a18734b9dd44f1b20928635df369ca))

## [1.0.7](https://gitlab.com/ardenlabs/cloudlogger.ts/compare/1.0.6...1.0.7) (2021-10-03)


### Bug Fixes

* corrected main entry file to dist/src/index.js ([2463c7a](https://gitlab.com/ardenlabs/cloudlogger.ts/commit/2463c7a4feeb06585775387f82d9bef0fbf2a887))

## [1.0.6](https://gitlab.com/ardenlabs/cloudlogger.ts/compare/1.0.5...1.0.6) (2021-09-29)


### Bug Fixes

* updated sr/changelog path to root. Git default release only includes root CHANGELOG.md. Not doc ([b8437fd](https://gitlab.com/ardenlabs/cloudlogger.ts/commit/b8437fdead69e20654319e27a784a7964d972b3a))
